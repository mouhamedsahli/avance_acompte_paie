#encoding: utf8
from openerp.osv import  osv, fields
from openerp import netsvc
from openerp import api
import openerp
from openerp import models, _
import calendar
import datetime

class hr_avance_salaire(osv.osv):
    _name = 'hr.avance_acompte_salaire'  
    

    def salaire_fn(self, cr, uid, ids, fields_name, args, context):
        res={} 
        avance_acompte_salaire = self.browse(cr, uid, ids, context)
        for record in avance_acompte_salaire :
            dt = datetime.date.today()
            dt = dt.replace(day = 1)
            d = datetime.date.today()
            contract_obj = self.pool.get('hr.contract')
            contract_ids = contract_obj.search(cr,uid,[('employee_id','=',record.employ_id.id)])
            contra_infos = contract_obj.read(cr, uid, contract_ids,['date_start','date_end','wage'])
            salaire = 0
            
#             datetime.datetime.strptime('24052010', "%d%m%Y").date()
            for info in contra_infos:
                start = info['date_start']
                year_start  = int(start[0:4]) 
                month_start = int(start[5:7] )
                day_start  = int(start[8:10] )
                date_start = datetime.date.today()
                date_start = date_start.replace(day = day_start)
                date_start = date_start.replace(year = year_start)
                date_start = date_start.replace(month = month_start)
                
                end = info['date_end']
                if end :
                    year  = int(end[0:4]) 
                    month = int(end[5:7] )
                    day  = int(end[8:10] )
                    date_end = datetime.date.today()
                    date_end = date_end.replace(day = day)
                    date_end = date_end.replace(year = year)
                    date_end = date_end.replace(month = month)
                    
                if not info['date_end']:
                    salaire = salaire + info['wage']
                else :
                    if date_start <= d <= date_end : 
                        salaire = salaire + info['wage']   

            res[record.id] = salaire
        return res


    def salaire_restant_fn(self, cr, uid, ids, fields_name, args, context):
        res={} 
        avance_acompte_salaire = self.browse(cr, uid, ids, context)
        for record in avance_acompte_salaire :
            dt = datetime.date.today()
            dt = dt.replace(day = 1)
            d = datetime.date.today()
            contract_obj = self.pool.get('hr.contract')
            contract_ids = contract_obj.search(cr,uid,[('employee_id','=',record.employ_id.id)])
            contra_infos = contract_obj.read(cr, uid, contract_ids,['date_start','date_end','wage'])
            salaire = 0
            
#             datetime.datetime.strptime('24052010', "%d%m%Y").date()
            for info in contra_infos:
                start = info['date_start']
                year_start  = int(start[0:4]) 
                month_start = int(start[5:7] )
                day_start  = int(start[8:10] )
                date_start = datetime.date.today()
                date_start = date_start.replace(day = day_start)
                date_start = date_start.replace(year = year_start)
                date_start = date_start.replace(month = month_start)
                
                end = info['date_end']
                if end :
                    year  = int(end[0:4]) 
                    month = int(end[5:7] )
                    day  = int(end[8:10] )
                    date_end = datetime.date.today()
                    date_end = date_end.replace(day = day)
                    date_end = date_end.replace(year = year)
                    date_end = date_end.replace(month = month)
                    
                if not info['date_end']:
                    salaire = salaire + info['wage']
                else :
                    if date_start <= d <= date_end : 
                        salaire = salaire + info['wage']   
  
            res[record.id] = salaire - record.employ_id.avance_encour - record.employ_id.acompte_encour - record.employ_id.opposition_encour
        return res

    def onchange_employ_id(self, cr, uid, ids, employ_id, context=None):
            res = {'value':{}}
            if not employ_id:
                return res
            
            d = datetime.date.today()
            contract_obj = self.pool.get('hr.contract')
            contract_ids = contract_obj.search(cr,uid,[('employee_id','=',employ_id)])
            contra_infos = contract_obj.read(cr, uid, contract_ids,['date_start','date_end','wage'])
            salaire = 0
            employee_obj = self.pool.get('hr.employee')
            employee_brw = employee_obj.browse(cr, uid, employ_id, context)
            for info in contra_infos:
                start = info['date_start']
                year_start  = int(start[0:4]) 
                month_start = int(start[5:7] )
                day_start  = int(start[8:10] )
                date_start = datetime.date.today()
                date_start = date_start.replace(day = day_start)
                date_start = date_start.replace(year = year_start)
                date_start = date_start.replace(month = month_start)
                
                end = info['date_end']
                if end :
                    year  = int(end[0:4]) 
                    month = int(end[5:7] )
                    day  = int(end[8:10] )
                    date_end = datetime.date.today()
                    date_end = date_end.replace(day = day)
                    date_end = date_end.replace(year = year)
                    date_end = date_end.replace(month = month)
                    
                if not info['date_end']:
                    salaire = salaire + info['wage']
                else :
                    if date_start <= d <= date_end : 
                        salaire = salaire + info['wage']   

            
            res['value'].update({'salaire': salaire })
            res['value'].update({'salaire_restant': salaire - employee_brw.avance_encour - employee_brw.acompte_encour - employee_brw.opposition_encour
       })
            return res
   
    _columns = {
        'name':fields.char('Description'),
        'employ_id':fields.many2one('hr.employee','Employé',required=True),
        
        'salaire':fields.function(salaire_fn, type="float", string='salaire'),
        'salaire_restant':fields.function(salaire_restant_fn, type="float", string='salaire restant'),

        'type_operation':fields.selection([
            ('avance','Avance'),
            ('acompte','Acompte'),
            ('opposition','Opposition')
            ],
        'Type opération', select=True,required=True),
        'amount':fields.float('Montant',required=True),
        'date':fields.date('Date ',required=True),
        'state':fields.selection([
            ('draft','Demande'),
            ('done','Accéptée'),
            ('cancel','Refusée'),
         ],'Demande', select=True, readonly=True),   
                
       
        }

    def _check_salair_restant(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
        if obj.amount > obj.salaire_restant:
            return False
        return True

    _constraints = [
        (_check_salair_restant, "le montant ne peut pas être supérieur au salaire restant !", ['amount']),
    ]
    
    def action_confirm(self,cr,uid,ids,context):
        self.write(cr,uid,ids,{'state':'done'})
        
    def action_cancel(self,cr,uid,ids,context):
        self.write(cr,uid,ids,{'state':'cancel'})
    
    _defaults={'state':'draft'}

hr_avance_salaire()


class hr_employee(osv.osv):
    _inherit = 'hr.employee'
    
    
    def _amount_avance_acompte_opp(self, cr, uid, ids, field_name, arg, context=None):
        cur_obj = self.pool.get('res.currency')
        res = {}
        for rec in self.browse(cr, uid, ids, context=context):
            avance_obj = self.pool.get('hr.avance_acompte_salaire')
            avance_ids = avance_obj.search(cr,uid,[('state','=','done'),('employ_id','=',rec.id)])
           
        return res    
    
    def avance_encour_fn(self, cr, uid, ids, fields_name, args, context):
        res={} 
        employees=self.browse(cr, uid, ids, context)
        for record in employees:
            d = datetime.date.today()
            month = d.month
            year =  d.year
            last_day = calendar.monthrange(year,month)

            date_begin = str(year) + '-' + str(month) +'-'+ '01'
            date_end =  str(year) + '-' + str(month) +'-'+ str(last_day[1])
            cr.execute("SELECT sum(amount) \
                  FROM hr_avance_acompte_salaire \
                  where employ_id = %s and state = 'done' and  type_operation = 'avance'\
                  and date between %s and %s  \
                  ; ", [record.id , date_begin , date_end])
            result = cr.fetchone()
            res[record.id] = result[0]
        return res

  
    def acompte_encour_fn(self, cr, uid, ids, fields_name, args, context):
        res={} 
        employees=self.browse(cr, uid, ids, context)
        for record in employees:
            d = datetime.date.today()
            month = d.month
            year =  d.year
            last_day = calendar.monthrange(year,month)

            date_begin = str(year) + '-' + str(month) +'-'+ '01'
            date_end =  str(year) + '-' + str(month) +'-'+ str(last_day[1])
            cr.execute("SELECT sum(amount) \
                  FROM hr_avance_acompte_salaire \
                  where employ_id = %s and state = 'done' and  type_operation = 'acompte'\
                  and date between %s and %s  \
                  ; ", [record.id , date_begin , date_end])
            result = cr.fetchone()
            res[record.id] = result[0]
        return res

  
    def opposition_encour_fn(self, cr, uid, ids, fields_name, args, context):
        res={} 
        employees=self.browse(cr, uid, ids, context)
        for record in employees:
            d = datetime.date.today()
            month = d.month
            year =  d.year
            last_day = calendar.monthrange(year,month)

            date_begin = str(year) + '-' + str(month) +'-'+ '01'
            date_end =  str(year) + '-' + str(month) +'-'+ str(last_day[1])
            
            cr.execute("SELECT sum(amount) \
                  FROM hr_avance_acompte_salaire \
                  where employ_id = %s and state = 'done' \
                   and  type_operation = 'opposition'\
                   and date between %s and %s  \
                  ; ", [record.id , date_begin , date_end])
            result = cr.fetchone()
            res[record.id] = result[0]
        return res

    
    _columns = { 
                'avance_encour':fields.function(avance_encour_fn, type="float", string='Avance'),
                'acompte_encour':fields.function(acompte_encour_fn, type="float", string='Acompte'),
                'opposition_encour':fields.function(opposition_encour_fn, type="float", string='Opposition'),     
    }
hr_employee()
