{
    "name": " Gestion Avance et acompte  sur salaire",
    "version": "1.0",
    "depends": ['hr','hr_contract'],
    "author": "RCA",
    "category": "Paie",
    "description": """
    Gestion ARSAT
    """,
    "init_xml": [],
    'data': [           
            "views/hr_view.xml",
            "views/hr_employe.xml",
           
            ],
           
    'demo_xml': [],
    'installable': True,
    'active': True,
}